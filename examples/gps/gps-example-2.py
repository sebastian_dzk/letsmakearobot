import io
import serial
import pynmea2

GPS_RECIVER_PORT = '/dev/ttyACM0'

global lastVTG

ser = serial.Serial(GPS_RECIVER_PORT, 9600, timeout=5.0)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

while 1:
    try:
        line = sio.readline()
        msg = pynmea2.parse(line)
        
        if isinstance(msg, pynmea2.types.talker.VTG):
           lastVTG = msg

        if isinstance(msg, pynmea2.types.talker.GGA):
           print('lat={}, lng={}, speed={}, alt={}'.format(msg.latitude, msg.longitude, lastVTG.spd_over_grnd_kmph, msg.altitude))
      
    except serial.SerialException as e:
        print('Device error: {}'.format(e))
        break

