import io
import serial
import pynmea2
import requests

GPS_RECIVER_PORT = '/dev/ttyACM0'
DEVICE_UID = '123456789012345'
GEOLOCATION_SERVER_API = 'https://pilnuj.com/app/open-api/position'

global lastVTG


def sendPosition(msg):
   if lastVTG:
      response = requests.post(GEOLOCATION_SERVER_API, json={"imei": DEVICE_UID, "lat" : msg.latitude, "lng" : msg.longitude, "velocity" : lastVTG.spd_over_grnd_kmph, "altitude" : msg.altitude})


ser = serial.Serial(GPS_RECIVER_PORT, 9600, timeout=5.0)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

while 1:
    try:
        line = sio.readline()
        msg = pynmea2.parse(line)
        
        if isinstance(msg, pynmea2.types.talker.VTG):
           lastVTG = msg

        if isinstance(msg, pynmea2.types.talker.GGA):
           sendPosition(msg)
      
    except serial.SerialException as e:
        print('Device error: {}'.format(e))
        break

