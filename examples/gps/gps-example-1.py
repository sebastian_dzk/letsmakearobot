import io
import serial
import pynmea2

GPS_RECIVER_PORT = '/dev/ttyACM0'

ser = serial.Serial(GPS_RECIVER_PORT, 9600, timeout=5.0)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

while 1:
    try:
        line = sio.readline()
        msg = pynmea2.parse(line)
        print(repr(msg))
      
    except serial.SerialException as e:
        print('Device error: {}'.format(e))
        break

